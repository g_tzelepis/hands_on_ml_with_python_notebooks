{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Polynomial Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**What if your data is actually more complex than a simple straight line?** Surprisingly, you can actually use a linear model to fit non linear data. A simple way to do this is to **add powers of each feature as new features**, then train a linear model on this extended set of features. This technique is called **Polynomial Regression**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s  look  at  an  example.  First,  let’s  generate  some  *non linear  data*,  based  on  a  simple **quadratic equation** :\n",
    "\n",
    "Note that:\n",
    "\n",
    "- While the **Normal Equation can only perform Linear Regression**, the Gradient Descent algorithms can be used to train many other models, as we will see.\n",
    "\n",
    "- A quadratic equation is of the form $y = ax^2 +bx +c$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = 100 #rows\n",
    "\n",
    "X = 6 * np.random.rand(m, 1) - 3\n",
    "y = 0.5 * X**2 + X + 2  + np.random.randn(m, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X, y, \"b.\")\n",
    "plt.xlabel(\"$x_1$\", fontsize=18)\n",
    "plt.ylabel(\"$y$\", rotation=45, fontsize=18)\n",
    "plt.axis([-3, 3, 0, 10])\n",
    "plt.title(\"Quadratic-looking data\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly, **a straight line will never fit this data properly**. So let’s use Scikit-Learn’s PolynomialFeatures  class  to  **transform  our  training  data**,  adding  the  **square  (2nd-degree polynomial)**  of  each  feature  in  the  training  set  as  new  features  (in  this  case  there  is just one feature):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import PolynomialFeatures"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_features = PolynomialFeatures(degree=2, include_bias=False)\n",
    "X_poly = poly_features.fit_transform(X)\n",
    "X[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_poly[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "X_poly now contains the original feature of X plus the square of this feature. Now you can fit a LinearRegression model to this extended training data "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LinearRegression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_reg = LinearRegression()\n",
    "lin_reg.fit(X_poly, y)\n",
    "lin_reg.intercept_, lin_reg.coef_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X, y, \"b.\", label=\"Quadratic-looking data\")\n",
    "\n",
    "#We are creating a line representing the prediction model\n",
    "#by having as a X equal 100 equal distributed points from -3 to 3 and their squares(X^2)\n",
    "#As y we take the  LinearRegression() predictions\n",
    "\n",
    "X_new = np.linspace(-3, 3, 100).reshape(100, 1)\n",
    "X_new_poly = poly_features.transform(X_new)\n",
    "y_new = lin_reg.predict(X_new_poly)\n",
    "\n",
    "plt.plot(X_new, y_new, \"r-\", linewidth=2, label=\"Predictions\")\n",
    "\n",
    "\n",
    "plt.xlabel(\"$x_1$\", fontsize=18)\n",
    "plt.ylabel(\"$y$\", rotation=0, fontsize=18)\n",
    "plt.legend(loc=\"upper left\", fontsize=14)\n",
    "plt.axis([-3, 3, 0, 10])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Markdown as md\n",
    "md(f\"Not bad: the model estimates: $𝑦̂ = {np.around(lin_reg.coef_[0][1], decimals=2)}x_1^2+ {np.around(lin_reg.coef_[0][0], decimals=2)}x_1 + {np.around(lin_reg.intercept_[0], decimals=2)}$ when the original function was $y = 0.5x_1^2+ 1.0x_1 + 2.0$ \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when there are multiple features, Polynomial Regression is capable of finding  relationships  between  features  (which  is  something  a  plain  Linear  Regressionmodel  cannot  do).  This  is  made  possible  by  the  fact  that  Polynomial Features  also adds  all  combinations  of  features  up  to  the  given  degree.  For  example,  if  there  were two  features  $a$  and  $b$,  PolynomialFeatures  with  **degree=3**  would  not  only  add  the features $a^2$, $a^3$, $b^2$, and $b^3$, but also the combinations $ab$, $a^2b$, and $ab^2$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**PolynomialFeatures(degree=d)** transforms an array containing $n$ features  into  an  array  containing  $\\frac{(n+d)!}{d!n!}$  features,  where  $n!$  is  the _factorial_ of $n$, equal to $1 \\times 2 \\times 3 \\times ⋯ \\times n$. **Beware of the combinatorial explosion of the number of features!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learning Curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If  you  perform  **high-degree  Polynomial  Regression**,  you  will  **likely  fit  the  training data much better than with plain Linear Regression**. For example, the following figure applies a  300-degree  polynomial  model  to  the  preceding  training  data,  and  compares  the result  with  a  pure  linear  model  and  a  quadratic  model  (2nd-degree  polynomial).Notice how the 300-degree polynomial model wiggles around to get as close as possile to the training instances."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualise it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import StandardScaler\n",
    "from sklearn.pipeline import Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for style, width, degree in ((\"g-\", 1, 300), (\"b--\", 2, 2), (\"r-+\", 2, 1)):\n",
    "    polybig_features = PolynomialFeatures(degree=degree, include_bias=False)\n",
    "    std_scaler = StandardScaler()\n",
    "    lin_reg = LinearRegression()\n",
    "    polynomial_regression = Pipeline([\n",
    "            (\"poly_features\", polybig_features),\n",
    "            (\"std_scaler\", std_scaler),\n",
    "            (\"lin_reg\", lin_reg),\n",
    "        ])\n",
    "    polynomial_regression.fit(X, y)\n",
    "    y_newbig = polynomial_regression.predict(X_new)\n",
    "    plt.plot(X_new, y_newbig, style, label=str(degree), linewidth=width)\n",
    "\n",
    "plt.plot(X, y, \"b.\", linewidth=3)\n",
    "plt.legend(loc=\"upper left\")\n",
    "plt.xlabel(\"$x_1$\", fontsize=18)\n",
    "plt.ylabel(\"$y$\", rotation=0, fontsize=18)\n",
    "plt.axis([-3, 3, 0, 10])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of  course,  this  high-degree  Polynomial  Regression  model  is  severely  **overfitting**  the training data, while the linear model is **underfitting** it. The model that will **generalize best**  in  this  case  is  the  **quadratic  model**.  It  makes  sense  since  the  data  was  generated using a quadratic model, but in general you won’t know what function generated the data,  so  how  can  you  decide  how  complex  your  model  should  be? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If  a  model  performs  well  on  the  training  data  but  generalizes  poorly according  to  the  cross-validation  metrics,  then  your  model  is  overfitting.  If  it  performs poorly on both, then it is underfitting. This is one way to tell when a model is **too simple** or **too complex**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another  way  is  to  look  at  the  **_learning  curves_**:  these  are  plots  of  the  model’s  performance on **the training set** and the **validation set** as a function of the training set size (or the training iteration). \n",
    "\n",
    "\n",
    "To generate the plots, simply train the model several times on  **different  sized  subsets**  of  the  training  set.  The  following  code  defines  a  function8that plots the learning curves of a model given some training data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import mean_squared_error\n",
    "from sklearn.model_selection import train_test_split"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_learning_curves(model, X, y):\n",
    "    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2)    \n",
    "    train_errors, val_errors = [], []\n",
    "    for m in range(1, len(X_train)):\n",
    "        model.fit(X_train[:m], y_train[:m])\n",
    "        y_train_predict = model.predict(X_train[:m])\n",
    "        y_val_predict = model.predict(X_val)\n",
    "        train_errors.append(mean_squared_error(y_train[:m], y_train_predict))\n",
    "        val_errors.append(mean_squared_error(y_val, y_val_predict))\n",
    "    plt.plot(np.sqrt(train_errors), \"r-+\", linewidth=2, label=\"train\")    \n",
    "    plt.plot(np.sqrt(val_errors), \"b-\", linewidth=3, label=\"val\")\n",
    "    plt.xlabel(\"Training Set Size\")\n",
    "    plt.ylabel(\"RMSE\", rotation=\"45\")\n",
    "    plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s look at the learning curves of the plain Linear Regression model (a straight line):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_reg = LinearRegression()\n",
    "plot_learning_curves(lin_reg, X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This deserves a bit of explanation:\n",
    "\n",
    "First, let’s look at the **performance on the training data**.  when  there  are  just  one  or  two  instances  in  the  training  set,  the  model  can  fit them perfectly, which is why the curve starts at zero **RMSE**. But as new instances are added to  the  training  set,  it  becomes  impossible  for  the  model  to  fit  the  training  data  perfectly, both because the data is **noisy** and because it is **not linear** at all. So the error on the training data goes up until it **reaches a plateau**, at which point adding new instances to the training set doesn’t make the average error much better or worse. \n",
    "\n",
    "Now let’s look  at  the  **performance  of  the  model  on  the  validation  data**.  When  the  model  is trained on very few training instances, it is incapable of generalizing properly, which is  why  the  validation  error (RMSE)  is  initially  quite  big.  Then  as  the  model  is  shown  more training examples, it learns and thus the validation error slowly goes down. However,once again a straight line cannot do a good job modeling the data, so the error **ends up at a plateau, very close to the other curve**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_These learning curves are typical of an underfitting model. Both curves have reacheda plateau; they are close and fairly high._**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*If your model is underfitting the training data, **adding more training examples will not help**. You need to use a more **complex model** or come up with **better features**.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let’s look at the learning curves of a **10th-degree** polynomial model on the same data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.pipeline import Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "polynomial_regression = Pipeline([\n",
    "    (\"poly_features\", PolynomialFeatures(degree=10, include_bias=False)),\n",
    "    (\"lin_reg\", LinearRegression()),   \n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_learning_curves(polynomial_regression, X, y)\n",
    "plt.axis([0, 80, 0, 3])           \n",
    "plt.show()                        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These learning curves look a bit like the previous ones, but there are **two very important differences**:\n",
    "\n",
    "- The  error  on  the  training  data  is  **lower**  than  with  the  Linear  Regression model.\n",
    "\n",
    "- There  is  a  **gap  between  the  curves**.  This  means  that  the  model  performs  significantly  better  on  the  training  data  than  on  the  validation  data,  which  is  the  hall‐mark  of  an  **overfitting  model**.  However,  if  you  used  a  much  larger  training  set,the two curves would continue to get closer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_One way to improve an overfitting model is to feed it more training data until the validation error reaches the training error._**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Bias/Variance Tradeoff\n",
    "\n",
    "An  important  theoretical  result  of  statistics  and  Machine  Learning  is  the  fact  that  a model’s  generalization  error  can  be  expressed  as  the  sum  of  three  very  different errors:\n",
    "\n",
    "\n",
    "*Bias*\n",
    "\n",
    "    This part of the generalization error is due to wrong assumptions, such as assuming that the data is linear when it is actually quadratic. A high-bias model is most likely to underfit the training data.\n",
    "    \n",
    "*Variance*\n",
    "\n",
    "    This  part  is  due  to  the  model’s  excessive  sensitivity  to  small  variations  in  the training data. A model with many degrees of freedom (such as a high-degree polynomial  model)  is  likely  to  have  high  variance,  and  thus  to  overfit  the  trainingdata.\n",
    "    \n",
    "*Irreducible error*\n",
    "\n",
    "    This  part  is  due  to  the  noisiness  of  the  data  itself.  The  only  way  to  reduce  this part of the error is to clean up the data (e.g., fix the data sources, such as brokensensors, or detect and remove outliers).\n",
    "    \n",
    "    \n",
    "**Increasing** a model’s complexity will typically **increase** its variance and **reduce** its bias.Conversely, reducing a model’s complexity increases its bias and reduces its variance. This is why it is called a tradeoff."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Regularized Linear Models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As  we  saw  previously,  a  good  way  to  reduce  overfitting  is  to  **regularize  the model** (i.e., to constrain it): the fewer degrees of freedom it has, the harder it will be for it to overfit the data. For example, a simple way to regularize a polynomial modelis to reduce the number of polynomial degrees.\n",
    "\n",
    "\n",
    "For a linear model, regularization is typically achieved by **constraining the weights of the model**. We will now look at **Ridge Regression**, **Lasso Regression**, and **Elastic Net**, which implement three different ways to constrain the weights."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ridge Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Ridge Regression** (also called **Tikhonov regularization**) is a regularized version of Linear Regression: a regularization term equal to $\\alpha\\sum_{i=1}^{n}= \\theta_i^2$ is added to the cost function. \n",
    "\n",
    "This  forces  the  learning  algorithm  **to  not  only  fit  the  data  but  also  keep  the  model weights  as  small  as  possible**.  Note  that  the  regularization  term  **should  only  be  added to the cost function during training**. Once the model is trained, you want to evaluatethe model’s performance **using the unregularized performance measure**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*It is quite common for the cost function used during training to be different  from  the  performance  measure  used  for  testing.  Apart from regularization, another reason why they might be different is that   a   good   training   cost   function   should   have   optimization-friendly  derivatives,  while  the  performance  measure  used  for  testing  should  be  as  close  as  possible  to  the  final  objective.  A  good example  of  this  is  a  classifier  trained  using  a  cost  function  such  asthe log loss but evaluated using precision/recall.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The hyperparameter $\\alpha$ controls how much you want to regularize the model. If $\\alpha = 0$ then Ridge Regression is just Linear Regression. If $\\alpha$ is very large, then all weights end up very close to zero and the result is a flat line going through the data’s mean. The following equation presents the Ridge Regression cost function:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ridge Regression cost function:\n",
    "\n",
    "(It is common to use the notation $J(\\theta)$ for cost functions that don’t have a short name)\n",
    "    \n",
    "\\begin{equation*}\n",
    "J(\\pmb{\\theta}) =\n",
    "MSE(\\pmb{\\theta}) + \\alpha\\frac{1}{2}\\sum_{i=1}^{n}\\theta_i^2\n",
    "\\end{equation*}\n",
    "\n",
    "\n",
    "**Note**  that  the  bias  term  $\\theta_0$  is  not  regularized  (the  sum  starts  at  $i = 1$,  not  $0$).  If  we define  $\\pmb{w}$  as  the  vector  of  feature  weights  ($\\theta_1$  to  $\\theta_n$),  then  the  regularization  term  is simply equal to $\\frac{1}{2}(||\\pmb{m}||_2)^2$, where $||\\pmb{m}||_2$ represents the $l_2$ norm of the weight vector. For Gradient Descent, just add $a\\pmb{w}$ to the MSE gradient vector (remember that the equation is :$\\nabla_\\theta MSE(\\pmb{\\theta})$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***It  is  important  to  scale  the  data  (e.g.,  using  a  StandardScaler)  before performing Ridge Regression, as it is sensitive to the scale ofthe input features. This is true of most regularized models.***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following figure shows several Ridge models trained on some linear data using different $\\alpha$ value.\n",
    "\n",
    "**On the left**, plain Ridge models are used, leading to linear predictions. \n",
    "\n",
    "**On the right**,  the  data  is  first  expanded  using  PolynomialFeatures(degree=10),  then  it  is scaled using a StandardScaler, and finally the Ridge models are applied to the resulting  features:  this  is  Polynomial  Regression  with  Ridge  regularization.  \n",
    "\n",
    "Note  how increasing  $\\alpha$  leads  to  **flatter**  (i.e.,  less  extreme,  more  reasonable)  predictions;  this **reduces the model’s variance** but **increases its bias**.As with Linear Regression, we can perform Ridge Regression either by computing a closed-form equation or by performing Gradient Descent. \n",
    "\n",
    "The pros and cons are the same. The equation under the graphs shows the closed-form solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Ridge"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(42)\n",
    "m = 20\n",
    "X = 3 * np.random.rand(m, 1)\n",
    "y = 1 + 0.5 * X + np.random.randn(m, 1) / 1.5\n",
    "X_new = np.linspace(0, 3, 100).reshape(100, 1)\n",
    "\n",
    "def plot_model(model_class, polynomial, alphas, **model_kargs):\n",
    "    for alpha, style in zip(alphas, (\"b-\", \"g--\", \"r:\")):\n",
    "        model = model_class(alpha, **model_kargs) if alpha > 0 else LinearRegression()\n",
    "        if polynomial:\n",
    "            model = Pipeline([\n",
    "                    (\"poly_features\", PolynomialFeatures(degree=10, include_bias=False)),\n",
    "                    (\"std_scaler\", StandardScaler()),\n",
    "                    (\"regul_reg\", model),\n",
    "                ])\n",
    "        model.fit(X, y)\n",
    "        y_new_regul = model.predict(X_new)\n",
    "        lw = 2 if alpha > 0 else 1\n",
    "        plt.plot(X_new, y_new_regul, style, linewidth=lw, label=r\"$\\alpha = {}$\".format(alpha))\n",
    "    plt.plot(X, y, \"b.\", linewidth=3)\n",
    "    plt.legend(loc=\"upper left\", fontsize=15)\n",
    "    plt.xlabel(\"$x_1$\", fontsize=18)\n",
    "    plt.axis([0, 3, 0, 4])\n",
    "\n",
    "plt.figure(figsize=(8,4))\n",
    "plt.subplot(121)\n",
    "plot_model(Ridge, polynomial=False, alphas=(0, 10, 100), random_state=42)\n",
    "plt.ylabel(\"$y$\", rotation=0, fontsize=18)\n",
    "plt.subplot(122)\n",
    "plot_model(Ridge, polynomial=True, alphas=(0, 10**-5, 1), random_state=42)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Closed-form equation:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\pmb{\\hat{\\theta}} =\n",
    "\\pmb{\\Bigl(X^TX + \\alpha\\mathrm{A}\\Bigl)^{-1}}\\pmb{X^Ty}\n",
    "\\end{equation*}\n",
    "\n",
    "Where: $\\pmb{A}$ is the $(n + 1) \\times (n + 1)$ $identity$ $matrix$, a square matrix full of $0$s except for $1$s on the main diagonal (top-left to bottom-right).  **Except** with a $0$ in the top-left cell, corresponding to the _bias term_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is how to perform Ridge Regression with Scikit-Learn **using a closed-form solution** (a variant of the above equation using a matrix factorization technique by _André-Louis Cholesky_):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Ridge"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ridge_reg = Ridge(alpha=1, solver=\"cholesky\")\n",
    "ridge_reg.fit(X, y)\n",
    "ridge_reg.predict([[1.5]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And using **Stochastic Gradient Descent**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import SGDRegressor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sgd_reg = SGDRegressor(penalty=\"l2\")\n",
    "sgd_reg.fit(X, y.ravel())\n",
    "sgd_reg.predict([[1.5]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The  penalty  hyperparameter  sets  the  type  of  regularization  term  to  use.  \n",
    "\n",
    "Specifying **\"l2\"**  indicates  that  you  want  SGD  to  add  a  regularization  term  to  the  cost  function  equal  to  half  the  square  of  the  $l_2$  norm  of  the  weight  vector:  this  is  simply  RidgeRegression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lasso Regression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:python_ml]",
   "language": "python",
   "name": "conda-env-python_ml-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
